"""
Programming for linguists

Implementation of the Reverse Polish Notation Converter
"""
from calculator.operator_ import Operator_
from calculator.reverse_polish_notation import ReversePolishNotation


class ReversePolishNotationCalculator:
    """
    Calculator of expression in Reverse Polish Notation
    """
    def __init__(self):
        pass

    def calculate(self, rpn_expression: ReversePolishNotation) -> float:
        """
        Main method of the ReversePolishNotationCalculator class.
        Calculate a result of expression in Reverse Polish Notation.

        :param rpn_expression: expression in Reverse Polish Notation Format
        :return:
        """

    def calculate_value(self, operator: Operator_):
        pass
